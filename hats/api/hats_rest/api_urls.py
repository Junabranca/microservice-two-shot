from django.urls import path
from .views import api_show_hat, api_list_hats


urlpatterns = [
    # to show a list of hats and create a hat
    path("hats/", api_list_hats, name="api_create_hats"),
    # to delete a hat, show details of a hat
    path("location/<int:location_vo_id>/hats/", api_list_hats, name="api_list_hats"),
    path("hats/<int:pk>/", api_show_hat, name="hat"),
]
