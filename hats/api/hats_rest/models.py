from django.db import models
from django.urls import reverse

class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True, null=True )
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()

# Create your models here.
class Hat(models.Model):
    name = models.CharField(max_length=20, unique=True)
    fabric = models.CharField(max_length=20)
    style = models.CharField(max_length=20)
    color = models.CharField(max_length=20)
    image = models.URLField(null=True)

    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE,
        null=True
    )


    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_show_hat", kwargs={"id": self.id})

    class Meta:
        ordering = ("name", "style", "fabric", "color", "image", "location")
