function ShoeList(props) {

    async function deleteShoe(shoe) {
        const deleteUrl = `http://localhost:8080${shoe.href}`
        const deleteResponse = await fetch (deleteUrl, {method: 'delete'})
        if (deleteResponse.ok) {
            console.log(`Deleted: ${shoe.model_name}`)
            alert(`Deleted: ${shoe.model_name}`)

        }
    }

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Model Name</th>
                    <th>Manufacturer</th>
                    <th>Color</th>
                    <th>Picture Url</th>
                    <th>Bin Closet Name</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                {props.shoes.map(shoe => {
                    return (
                        <tr key={shoe.href}>
                            <td>{ shoe.model_name }</td>
                            <td>{ shoe.manufacturer }</td>
                            <td>{ shoe.color }</td>
                            <td>{ shoe.picture_url }</td>
                            {/* <td> <img src={shoe.picture_url} width="200" height="200" alt="shoe" /></td> */}
                            <td>{ shoe.bin }</td>
                            <td>
                                <button type="button" className="btn btn-danger" onClick={() => deleteShoe(shoe)}>Delete</button>
                            </td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    )
}

export default ShoeList;