import React, {useState } from 'react';


function HatList(props) {

async function deleteHat(hat) {
  const deleteURL = `http://localhost:8090/api/hats/${hat.id}`
  const deleteResponse = await fetch(deleteURL, {method: 'delete'})
  if (deleteResponse.ok) {
    alert(`You just deleted : ${hat.name} please refresh the page`)
  }
}

      return (
          <table className="table">
            <thead>
              <tr>
                  <th>Name</th>
                  <th>Fabric</th>
                  <th>Style</th>
                  <th>Color</th>
                  <th>Image</th>
                  <th>Location</th>
              </tr>
              </thead>
              <tbody>
                  {props.hats?.map(hat => { return (
                      <tr key={hat.name}>
                          <td>{hat.name}</td>
                          <td>{hat.fabric}</td>
                          <td>{hat.style}</td>
                          <td>{hat.color}</td>
                          <td><img src={hat.image} width="200" height="150" alt="hat" /></td>
                          <td>{hat.location.closet_name}</td>
                          <td className="align-middle">
                          <button onClick={() => deleteHat(hat)} type = "button" className="btn btn-danger">Delete</button>
                          </td>
                      </tr>
                    );
              })}
              </tbody>
          </table>
    );
}
export default HatList;
