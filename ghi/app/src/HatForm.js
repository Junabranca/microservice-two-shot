import React, { useEffect, useState } from 'react';

function HatForm(props){

    const [locations, setLocations] = useState([]);

      const fetchData = async () => {
          const url = 'http://localhost:8100/api/locations/';
          const response = await fetch(url);

          if(response.ok){
              const data = await response.json();
              setLocations(data.locations);
      }
}
const[name, setName] = useState('');

const handleNameChange = (event) => {
  const value = event.target.value;
  setName(value);
}

const[fabric, setFabric] = useState('');

const handleFabricChange = (event) => {
  const value = event.target.value;
  setFabric(value);
}

const[style, setStyle] = useState('');

const handleStyleChange = (event) => {
  const value = event.target.value;
  setStyle(value);
}

const[color, setColor] = useState('');

const handleColorChange = (event) => {
  const value = event.target.value;
  setColor(value);
}

const [image, setImage] = useState('');

const handleImageChange = (event) => {
  const value = event.target.value;
  setImage(value);
}

const [location, setLocation] = useState('');

const handleLocationChange = (event) => {
  const value = event.target.value;
  setLocation(value);
}

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.name = name;
    data.fabric = fabric;
    data.style = style;
    data.color = color;
    data.image = image;
    data.location = location;

    const hatUrl = `http://localhost:8090/api/hats/`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
          'Content-Type': 'application/json',
      },
  };
    const response = await fetch(hatUrl, fetchConfig);
    if (response.ok) {
        const newHat = await response.json();
        console.log(newHat);

        setName('');
        setFabric('');
        setStyle('');
        setColor('');
        setImage('');
        setLocation('');
    }
}
useEffect(() => {
      fetchData();
}, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new hat</h1>
            <form onSubmit={handleSubmit} id="create-hat-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFabricChange} value={fabric} placeholder="Fabric" required type="text" name="fabric"  id="fabric" className="form-control"/>
                <label htmlFor="starts">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStyleChange} value={style}  placeholder="Style" required type="text" name="style"  id="style" className="form-control"/>
                <label htmlFor="starts">Style</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleColorChange} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="description">Color</label>
                <label htmlFor="description">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleImageChange} value={image} placeholder="Image URL" required type="url" name="image"  id="image" className="form-control"/>
                <label htmlFor="room_count">Image URL</label>
              </div>
              <div className="form-floating mb-3">
                <select onChange={handleLocationChange} value={location} required name="location" id="location" className="form-select">
                  <option value="">Choose a wardrobe</option>
                  {locations.map(location => {
                    return (
                      <option key={location.id} value={location.href}>
                        {location.closet_name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create Hat</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default HatForm;
