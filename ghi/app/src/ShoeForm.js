import React, {useEffect, useState} from 'react';

function ShoeForm () {
    const [bins, setBins] = useState([]);

    const [bin, setBin] = useState('');
    const [modelName, setModelName] = useState('');
    const [manufacturer, setManufacturer] = useState('');
    const [color, setColor] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');

    const handleBinChange = (event) => {
        const value = event.target.value;
        setBin(value);
    }

    const handleModelNameChange = (event) => {
        const value = event.target.value;
        setModelName(value);
    }

    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handlePictureUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.bin_id = bin;
        data.model_name = modelName;
        data.manufacturer = manufacturer;
        data.color = color;
        data.picture_url = pictureUrl;


        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        console.log(data)
        const response = await fetch(shoeUrl, fetchConfig);
        console.log(response)
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe)


            setBin('');
            setModelName('');
            setManufacturer('');
            setColor('');
            setPictureUrl('');
        }
    }

    const fetchData = async () => {
        const binUrl = 'http://localhost:8100/api/bins/';
        const response = await fetch(binUrl);

        if (response.ok) {
            const data = await response.json();
            const shoeData = data.bins
            setBins(shoeData);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);


    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new shoe</h1>
            <form onSubmit={handleSubmit} id="create-shoe-form">
              <div className="form-floating mb-3">
                <input
                    onChange={handleModelNameChange}
                    placeholder="Model Name"
                    required
                    type="text"
                    value={modelName}
                    name="model_name"
                    id="model_name"
                    className="form-control"
                />
                <label htmlFor="model_name">Model Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                    onChange={handleManufacturerChange}
                    placeholder="Manufacturer"
                    required
                    type="text"
                    value={manufacturer}
                    name="manufacturer"
                    id="manufacturer"
                    className="form-control"
                />
                <label htmlFor="manufacturer">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input
                    onChange={handleColorChange}
                    placeholder="Color"
                    required
                    type="text"
                    value={color}
                    name="color"
                    id="color"
                    className="form-control"
                />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input
                    onChange={handlePictureUrlChange}
                    placeholder="Picture Url"
                    required
                    type="text"
                    value={pictureUrl}
                    name="picture_url"
                    id="picture_url"
                    className="form-control"
                />
                <label htmlFor="picture_url">Picture Url</label>
              </div>
              <div className="mb-3">
                <select
                    onChange={handleBinChange}
                    required
                    value={bin}
                    name="bin"
                    id="bin"
                    className="form-select"
                >
                  <option value="">Choose a bin</option>
                  {bins.map(bin => {
                    return (
                        <option key={bin.id} value={bin.id}>
                            {bin.closet_name}
                        </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}

export default ShoeForm;
