# Wardrobify

Team:

* Person 1 - Juna - Hat microservice
* Person 2 - Jackie Liu: Shoes microservice

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

Track manufacturer, model name, color, URL for picture, and bin where it exists.
Allow users to see a list of shoes, create, update and delete each shoe on the list.


## Hats microservice

hats/api
- Install Hat App into Settings.py of the Hat project
- create URLS.py
    - Restful API's
        - GET list of all hats /api/hats/
        - POST create a new hat /api/hats/
        - DELETE a single hat /api/hats/<int:id>/

hats/poll
- implement the script file in hats/poll/poller to pull the Location Data from wardrobe/api/wardrobe_api
